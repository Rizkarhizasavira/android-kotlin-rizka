package com.bcas.restplayground

import android.os.Bundle
import android.telecom.Call
import androidx.appcompat.app.AppCompatActivity
import com.bcas.restplayground.ProductsActivity.Companion.KEY_ID
import com.bcas.restplayground.databinding.ActivityDetailProductBinding
import com.bumptech.glide.Glide
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class DetailProductActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailProductBinding

    @Inject
    lateinit var productWebService: ProductWebService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailProductBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val id = intent.getIntExtra(KEY_ID, 0)
        val callback = object : Callback<ProductResponse> {
            override fun onResponse(
                call: retrofit2.Call<ProductResponse>,
                response: Response<ProductResponse>
            ) {
                val body = response.body()
                if (body != null) {
                    Glide.with(binding.root.context)
                        .load(body.image)
                        .circleCrop()
                        .into(binding.ivDetailImage)
                }
            }

            override fun onFailure(call: retrofit2.Call<ProductResponse>, t: Throwable) {
                t.printStackTrace()
            }
        }
        productWebService.getSingleProduct(id).enqueue(callback)
    }
}

