package com.bcas.restplayground

import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BindingActivity<B: ViewBinding> : AppCompatActivity() {

    protected lateinit var binding: B

}