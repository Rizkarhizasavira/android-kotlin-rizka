package rizka.bcas.projectsavira2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import rizka.bcas.projectsavira2.adapter.ProdukAdapter
import rizka.bcas.projectsavira2.adapter.PromoAdapter
import rizka.bcas.projectsavira2.databinding.ActivityPromoBinding
import rizka.bcas.projectsavira2.model.ProdukModel
import rizka.bcas.projectsavira2.model.PromoModel

class HomeMainActivity : AppCompatActivity(){
        private lateinit var binding: ActivityPromoBinding
//    private val mainAdapter = ProdukAdapter()



        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            binding = ActivityPromoBinding.inflate(layoutInflater)
            setContentView(binding.root)

//        val mainAdapter = HomeMainAdapter(
//            dataNews = populateData(),
//            onClickNews = {dataNews ->
//                DetailNewsActivity.navigateToActivityDetail(this,dataNews)
//        })


//        binding.rvProduk.adapter = mainAdapter

            val promoAdapter = PromoAdapter(populateDataForPromo())
            binding.rvListHorizontal.adapter = promoAdapter

            val produkAdapter = ProdukAdapter(populateData())
            binding.rvProduk.adapter = produkAdapter

//        val data = populateData()
//        mainAdapter.addDataNews(data)

//        mainAdapter.addOnClickNews { dataNews ->
//            DetailNewsActivity.navigateToActivityDetail(
//                activity = this, dataNews)
//        }
//
//        binding.btnAdd.setOnClickListener{
//            val newData = populateData2()
//            mainAdapter.addDataNews(newData)
//        }
        }

        private fun populateData() : List<ProdukModel>{
            val listData = listOf(
                ProdukModel(
                    image = R.drawable.kkb_ib_bcas,
                    title = "Harpitnas jadiin lah bos biar enjoyyy",
                    subtitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint " +
                            "occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                ),
                ProdukModel(
                    image = R.drawable.kkb_ib_bcas,
                    title = "Barcelona mengalami kekalahan pada liga malam jumat",
                    subtitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint " +
                            "occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

                ),
                ProdukModel(
                    image = R.drawable.ic_launcher_background,
                    title = "Marvel terus mengembangkan film-film terbarunya",
                    subtitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint " +
                            "occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                ),
            )
            return listData
        }

//    private fun populateData2(): List<NewsModel>{
//        return listOf(
//            NewsModel(
//                image = R.drawable.berita_harpitnas,
//                title = "Harpitnas jadiin lah bos biar enjoyyy",
//                subtitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint " +
//                        "occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
//            ),
//            NewsModel(
//                image = R.drawable.berita_barca,
//                title = "Barcelona mengalami kekalahan pada liga malam jumat",
//                subtitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint " +
//                        "occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
//
//            ),
//            NewsModel(
//                image = R.drawable.ic_launcher_background,
//                title = "Marvel terus mengembangkan film-film terbarunya",
//                subtitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint " +
//                        "occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
//            )
//        )
//    }

        private fun populateDataForPromo() : List<PromoModel>{
            val listData = listOf(
                PromoModel(
                    title = "Promo 1",
                    subtitle = "promo menggiurkan di hari raya",
                    image = R.drawable.promo_bcas
                ),
                PromoModel(
                    title = "Promo 2",
                    subtitle = "promo menggiurkan di hari libur",
                    image = R.drawable.promo_bcas
                ),
                PromoModel(
                    title = "Promo 3",
                    subtitle = "promo menggiurkan di hari kerja",
                    image = R.drawable.promo_bcas
                ),
                PromoModel(
                    title = "Promo 4",
                    subtitle = "promo menggiurkan di hari merdeka",
                    image = R.drawable.promo_bcas
                ),
                PromoModel(
                    title = "Promo 5",
                    subtitle = "promo menggiurkan di hari lebaran",
                    image = R.drawable.promo_bcas
                ),
            )
            return listData
        }
    }
