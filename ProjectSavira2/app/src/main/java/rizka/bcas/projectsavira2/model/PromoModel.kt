package rizka.bcas.projectsavira2.model

import android.icu.text.CaseMap.Title
import android.media.Image
import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class PromoModel(
    val image: Int?,
    val title: String?,
    val subtitle: String?
) : Parcelable
