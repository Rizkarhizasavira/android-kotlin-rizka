package com.example.myapplication1.profile

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication1.R
import com.example.myapplication1.base.BaseDialog
import com.example.myapplication1.databinding.ActivityProfileBinding
import com.example.myapplication1.view.LoginActivity
import com.example.myapplication1.view.LoginActivity.Companion.KEY_ADDRESS
import com.example.myapplication1.view.LoginActivity.Companion.KEY_INPUT
import com.example.myapplication1.view.LoginActivity.Companion.KEY_NAME

class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val name = intent.getStringExtra(KEY_NAME)
        val address = intent.getStringExtra(KEY_ADDRESS)
        val valueinput = intent.getStringExtra(KEY_INPUT)

        binding.tvName.text = valueinput
        binding.tvAddress.text = address

        logout()
    }

    private fun logout() {
        binding.btnLogout.setOnClickListener {
            val dialog =
                BaseDialog(
                    this, "Peringatan", "yakin ingin keluar aplikasi?", onclicked = {
//                    Toast.makeText(this, "ini button proceed", Toast.LENGTH_SHORT).show()
                        finish()
//                    untuk ke page selanjutnya
                        startActivity(Intent(this, LoginActivity::class.java))
                    },
                    withImage = false,
//                    kalo withimage true dan gapake imagenya itu bakal pake yang default
//                    kalo withimage nya false ga akan keluar imagenya
                    image = R.drawable.ic_launcher_background
                )
            dialog.setCancelable(false)
            dialog.show()
//            BaseDialog(this).setCancelable(false).show()
        }

    }
}