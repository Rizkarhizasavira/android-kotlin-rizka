package com.example.myapplication1

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication1.databinding.TugasLayout1Binding
import com.example.myapplication1.view.LoginActivity

class HomeActivity:AppCompatActivity() {

    private lateinit var binding: TugasLayout1Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TugasLayout1Binding.inflate(layoutInflater)
        setContentView(binding.root)

//        Untuk mengubah halaman satu ke halaman selanjutnya
        binding.btn1.setOnClickListener(){
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }
        binding.btn2.setOnClickListener(){
            val intent2 = Intent(this,Calculator::class.java)
            startActivity(intent2)
        }
        binding.btn3.setOnClickListener(){
            val intent3 = Intent(this, LoginActivity::class.java)
            startActivity(intent3)
        }
    }

}