package com.example.myapplication1.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize

data class NewsModel(
    val image: Int?,
    val title: String?,
    val description: String?
) : Parcelable
