package com.example.myapplication1.view.home

import android.service.autofill.OnClickAction
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication1.databinding.ActivityDetailNewsBinding
import com.example.myapplication1.databinding.ItemCategoryBinding
import com.example.myapplication1.model.CategoryModel

class CategoryAdapter(
    private val dataMenu: List<CategoryModel>
) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    inner class CategoryViewHolder(val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindView(data: CategoryModel) {
            binding.tvCategory.text = data.title
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder = CategoryViewHolder(
            ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,false)
            )


    override fun getItemCount(): Int = dataMenu.size

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bindView(dataMenu[position])
    }


}