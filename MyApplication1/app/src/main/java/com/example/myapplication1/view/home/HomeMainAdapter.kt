package com.example.myapplication1.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication1.databinding.ItemNewsBinding
import com.example.myapplication1.model.NewsModel

class HomeMainAdapter :
    RecyclerView.Adapter<HomeMainAdapter.HomeMainViewHolder>() {

    private var dataNews : MutableList<NewsModel> = mutableListOf()
    private var onClickNews: (NewsModel) -> Unit = {}

    fun addDataNews(newData: List<NewsModel>){
        dataNews.addAll(newData)
        notifyDataSetChanged()
    }

    fun addOnClickNews(clickNews: (NewsModel) -> Unit){
        onClickNews = clickNews
    }

    inner class HomeMainViewHolder(val binding: ItemNewsBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data:NewsModel, onClickNews: (NewsModel) -> Unit) {
            binding.ivItemNews.setImageResource(data.image ?: 0)
            binding.tvTitleNews.text = data.title
            binding.tvSubtitleNews.text = data.description

            binding.llNews.setOnClickListener {
                onClickNews(data)

            }


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeMainViewHolder =
        HomeMainViewHolder(
            ItemNewsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: HomeMainViewHolder, position: Int) {
        println("render position-> $position")
        holder.bindView(dataNews[position], onClickNews)

    }

    override fun getItemCount(): Int = dataNews.size

}