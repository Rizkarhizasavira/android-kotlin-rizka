package com.example.myapplication1.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication1.databinding.ActivityDetailNewsBinding
import com.example.myapplication1.databinding.ActivityHomeBinding
import com.example.myapplication1.model.NewsModel

class DetailNewsActivity: AppCompatActivity() {
        private lateinit var binding: ActivityDetailNewsBinding

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            binding = ActivityDetailNewsBinding.inflate(layoutInflater)
            setContentView(binding.root)

            setDataToViewDetail()

    }

    private fun setDataToViewDetail() {
        val data = intent.getParcelableExtra<NewsModel>(DATA_NEWS)
        binding.tvDetailTitleNews.text = data?.title
        binding.tvDetailNews.text = data?.description
        binding.ivDetailNews.setImageResource(data?.image ?: 0)

        binding.componentAppbar.tvAppbar.text = "Detail News"
        binding.componentAppbar.ivBack.setOnClickListener {
            this.onBackPressed()
        }
    }

    companion object {
        private const val DATA_NEWS = "DATA_NEWS"
        fun navigateToActivityDetail(activity: Activity, dataNews: NewsModel) {
            val intent = Intent(activity, DetailNewsActivity::class.java)
            intent.putExtra(DATA_NEWS, dataNews)
            activity.startActivity(intent)
        }
    }
}