package com.example.myapplication1.view.latihanfragment.inputbiodata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.myapplication1.R
import com.example.myapplication1.databinding.InputBiodataFragmentBinding

class InputBiodataFragment : Fragment() {

    private lateinit var binding: InputBiodataFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = InputBiodataFragmentBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnRegister.setOnClickListener {

            handleRegisterButton()
        }

        setAppBar()


    }

    private fun setAppBar(){
        binding.layoutComponentAppBar.tvAppbar.text = "Register Profile"
        binding.layoutComponentAppBar.ivBack.setOnClickListener {
            requireActivity()
                .onBackPressed()
        }
    }

    private fun handleRegisterButton() {
        val checkEtName = binding.etNama.text.isNullOrEmpty().not()
        val checkEtEmail = binding.etEmail.text.isNullOrEmpty().not()
        val checkEtNomerHp = binding.etNomerHp.text.isNullOrEmpty().not()
        val checkEtAlamat = binding.etAlamat.text.isNullOrEmpty().not()

        if (checkEtName && checkEtAlamat && checkEtEmail && checkEtNomerHp) {
            val bundle = Bundle().apply {
                putString(NAME_KEY, binding.etNama.text.toString())
                putString(EMAIL_KEY, binding.etEmail.toString())
                putString(NOHP_KEY, binding.etNomerHp.toString())
                putString(ALAMAT_KEY, binding.etAlamat.toString())

            }
            val successFragment = SuccessBiodataFragment().apply {
                arguments = bundle

            }
            parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, successFragment)

                .commit()
        } else {
            Toast.makeText(context, "Tolong lengkapi biodata", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        const val NAME_KEY = "nama"
        const val EMAIL_KEY = "email"
        const val NOHP_KEY = "no hp"
        const val ALAMAT_KEY = "alamat"

    }
}
