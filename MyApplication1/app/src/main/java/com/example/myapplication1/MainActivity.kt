package com.example.myapplication1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.myapplication1.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

//
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
//        var name = "John Doe"
//        Toast.makeText(applicationContext, name, Toast.LENGTH_LONG).show()
        val names = "Hello Rizka"
        val ages = "Age: 24"
        val addresses = "Address: Jakarta Pusat"
        val emails = "Email: rizkarhizasavira@gmail.com"
        val numbers = "Phone number: 0871122334"
        val nicknames = "Nickname: Savira"
        val genders = "Gender: Female"
        val births = "DOB: 31 Mei 1998"
        binding.hello.text = names
        binding.age.text = ages
        binding.address.text = addresses
        binding.email.text = emails
        binding.number.text = numbers
        binding.nickname.text = nicknames
        binding.gender.text = genders
        binding.birth.text = births



    }
}