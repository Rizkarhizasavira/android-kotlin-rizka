package com.example.myapplication1.view.home

import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication1.R
import com.example.myapplication1.databinding.ActivityHomeBinding
import com.example.myapplication1.model.CategoryModel
import com.example.myapplication1.model.NewsModel
import com.example.myapplication1.view.DetailNewsActivity
import kotlin.contracts.Returns

class HomeMainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding
    private val mainAdapter = HomeMainAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.componentAppbar.tvAppbar.text =  "Recycle view Activity"
//        binding.componentAppbar.ivBack.visibility = View.GONE
        binding.componentAppbar.ivBack.setOnClickListener{
            this.onBackPressed()
        }

//        val mainAdapter = HomeMainAdapter(dataNews = populateData(),
//            onClickNews = {
//                DetailNewsActivity.navigateToActivityDetail(this, it)
//
//            }
//        )

        binding.rvNewsNew.adapter = mainAdapter

        val categoryAdapter = CategoryAdapter(dataMenu = populateDataForCategory())
        binding.rvListHorizontal.adapter = categoryAdapter

        val data = populateData()
        mainAdapter.addDataNews(data)

        mainAdapter.addOnClickNews { dataNews ->
            DetailNewsActivity.navigateToActivityDetail(
                activity = this, dataNews
            )

            binding.btnAddNews.setOnClickListener {
                val newData = populateData2()
                mainAdapter.addDataNews(newData)
            }

        }
    }

    private fun populateData2(): List<NewsModel> {
        return listOf(
            NewsModel(
                image = R.drawable.ilustrasi_banjir,
                title = "Hujan Terun nih",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            NewsModel(
                image = R.drawable.ilustrasi_banjir,
                title = "Hujan Terun nih",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            NewsModel(
                image = R.drawable.ilustrasi_banjir,
                title = "Hujan Terun nih",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

            ),
        )

    }

    private fun populateData(): List<NewsModel> {
        val listData = listOf(
            NewsModel(
                image = R.drawable.ilustrasi_banjir,
                title = "Hujan Terun nih",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            NewsModel(
                image = R.drawable.ilustrasi_banjir,
                title = "Hujan Terun nih",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            NewsModel(
                image = R.drawable.ilustrasi_banjir,
                title = "Hujan Terun nih",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

            ),
        )

        return listData
    }

    private fun populateDataForCategory(): List<CategoryModel> {
        val listData = listOf(
            CategoryModel(
                title = "News",
            ),

            CategoryModel(
                title = "Sport",
            ),

            CategoryModel(
                title = "Religi",
            ),

            CategoryModel(
                title = "Fashion",
            ),
            CategoryModel(
                title = "Teens",
            ),
            CategoryModel(
                title = "Watch",
            ),
        )

        return listData
    }

}