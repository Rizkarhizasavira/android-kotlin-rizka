package com.example.myapplication1.view

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.os.strictmode.WebViewMethodCalledOnWrongThreadViolation
import android.telecom.CallScreeningService
import android.view.View
import android.widget.Toast
import android.window.SplashScreen
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication1.databinding.ActivityCalculatorBinding
import com.example.myapplication1.databinding.ActivityLoginBinding
import com.example.myapplication1.profile.ProfileActivity

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    private val email = "ir@gmail.com"
    private val name = "name"
    private val address = "address"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)




        binding.btnSubmit.setOnClickListener {
//            val inputEmail = binding.etEmail.text.toString()
//            navigateScreenwithInput(ProfileActivity::class.java, inputEmail)
//            if (inputEmail == email) {
//                Toast.makeText(applicationContext, "Berhasil Login", Toast.LENGTH_SHORT).show()
//                navigateScreen(ProfileActivity::class.java)
//            } else {
//                Toast.makeText(applicationContext, "Gagal Login", Toast.LENGTH_SHORT).show()
//            }

            showLoading()
        }

        binding.pbLoading.setOnClickListener{
            hideLoading()
        }
    }

    private fun navigateScreen (screen: Class<*>) {
        val intent = Intent(applicationContext, screen)
        intent.putExtra(KEY_NAME, name)
        intent.putExtra(KEY_ADDRESS, address)
        startActivity(intent)
    }

    private fun showLoading() {

        binding.pbLoading.visibility = View.VISIBLE
        binding.btnSubmit.visibility = View.GONE
    }

    private fun hideLoading() {

        binding.btnSubmit.visibility = View.GONE
        binding.pbLoading.visibility = View.VISIBLE

    }

    private fun navigateScreenwithInput (screen: Class<*>, input: String) {
        val intent = Intent(applicationContext, screen)
        intent.putExtra(KEY_INPUT, input)
        intent.putExtra(KEY_ADDRESS, address)
        startActivity(intent)
    }

    companion object {
        const val KEY_NAME = "name"
        const val KEY_ADDRESS = "address"
        const val KEY_INPUT = "input"

    }

}
