package com.example.myapplication1.view.latihanfragment.inputbiodata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myapplication1.databinding.SuccessRegisterFragmentBinding
import com.example.myapplication1.view.latihanfragment.inputbiodata.InputBiodataFragment.Companion.NAME_KEY

class SuccessBiodataFragment : Fragment() {

    private lateinit var binding: SuccessRegisterFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SuccessRegisterFragmentBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setDataFromArguments()

    }

    private fun setDataFromArguments() {
        var getNameFromArgument = arguments?.getString(NAME_KEY)
        binding.tvNamaSukses.text = getNameFromArgument

    }
}