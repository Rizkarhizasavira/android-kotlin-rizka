package com.example.myapplication1.view.latihanfragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication1.R
import com.example.myapplication1.databinding.ActivityHostBinding
import com.example.myapplication1.view.latihanfragment.home.HomeFragment
import com.example.myapplication1.view.latihanfragment.inputbiodata.InputBiodataFragment

class HostActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHostBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHostBinding.inflate(layoutInflater)
        setContentView(binding.root)

        homeFragment()
    }

//    private fun initBiodataFragment() {
//        val inputBiodataFragment = InputBiodataFragment()
//        supportFragmentManager.beginTransaction()
//            .replace(R.id.fragment_container, inputBiodataFragment)
//            .commit()


    private fun homeFragment() {
        val inputBiodataFragment = HomeFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, inputBiodataFragment)
            .commit()
    }
}