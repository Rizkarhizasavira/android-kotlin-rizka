package rizka.bcas.projectsavira

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import rizka.bcas.projectsavira.Home.HomeMainActivity
import rizka.bcas.projectsavira.databinding.ActivityLoginBinding

class LoginActivity: AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        val email="Rizkarhizasavira@gmail.ocm"
        val password="1234"
        val name="Rizka Rhiza Savira"
        val bod="31 Mei 1998"
        val address="Jakarta Pusat"
        val phone="08793457838"
        super.onCreate(savedInstanceState)
        binding= ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.SignUpText.setOnClickListener {
            val intent=Intent(applicationContext, RegisterActivity::class.java)
            startActivity(intent)
        }

        binding.loginButton.setOnClickListener {



            if (email==email){
                if(password==password){

                    val bundle = Bundle()
                    bundle.putString(KEY_NAME,name)
                    bundle.putString(KEY_EMAIL,email)
                    bundle.putString(KEY_ADDRESS,address)
                    bundle.putString(KEY_BOD,bod)
                    bundle.putString(KEY_PHONE,phone)
                    bundle.putString(KEY_PASSWORD,password)

                    navigationScreenWithInput2(HomeMainActivity::class.java,bundle)
                    // navigationScreenWithInput(BiodataActivity::class.java,email,password)
                    Toast.makeText(applicationContext, "Login Success", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(applicationContext, "Login Failed", Toast.LENGTH_SHORT).show()
                }


            }


        }

    }

    private fun navigationScreenWithInput2(screen: Class<*>, bundle: Bundle){
        val intent=Intent(applicationContext,screen)
        intent.putExtras(bundle)
        startActivity(intent)

    }
    private fun navigationScreenWithInput(screen: Class<*>, inputEmail:String, inputPassword:String){
        val intent= Intent(applicationContext,screen)
        intent.putExtra(KEY_EMAIL,inputEmail)
        intent.putExtra(KEY_PASSWORD,inputPassword)
        startActivity(intent)

    }

    companion object{
        const val KEY_NAME="name"
        const val KEY_PASSWORD="password"
        const val KEY_EMAIL="email"
        const val KEY_ADDRESS="address"
        const val KEY_PHONE="phone"
        const val KEY_BOD="bod"

    }

}