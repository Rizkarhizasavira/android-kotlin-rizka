package rizka.bcas.projectsavira

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import rizka.bcas.projectsavira.LoginActivity.Companion.KEY_ADDRESS
import rizka.bcas.projectsavira.LoginActivity.Companion.KEY_EMAIL
import rizka.bcas.projectsavira.LoginActivity.Companion.KEY_NAME
import rizka.bcas.projectsavira.LoginActivity.Companion.KEY_PASSWORD
import rizka.bcas.projectsavira.base.BaseDialog
import rizka.bcas.projectsavira.databinding.ActivityBiodataBinding

class BiodataActivity: AppCompatActivity() {
    private lateinit var binding:ActivityBiodataBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityBiodataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val intenData= intent.extras

        val nama = intenData?.getString(KEY_NAME)
        val email = intenData?.getString(KEY_EMAIL)
        val address = intenData?.getString(KEY_ADDRESS)

        binding.tvEmail.text = email.toString()
        binding.tvAddress.text = address.toString()
        binding.tvName.text = nama.toString()

        logout()
    }
    private fun logout(){
        binding.btnLogout.setOnClickListener {
            val dialog= BaseDialog(this,
                "Warning !",
                "Are you sure want to logout?",
                onClicked = {
                    val intent= Intent(applicationContext, LoginActivity::class.java)
                    startActivity(intent)
//                Toast.makeText(applicationContext,"ini button procced",Toast.LENGTH_LONG).show()
                    //finish()
                }, withImage = false,
                image = R.drawable.ic_launcher_background
            )
            dialog.setCancelable(false)
            dialog.show()
        }


    }

}