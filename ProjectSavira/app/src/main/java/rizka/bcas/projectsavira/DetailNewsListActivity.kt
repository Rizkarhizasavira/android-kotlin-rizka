package rizka.bcas.projectsavira

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import rizka.bcas.projectsavira.databinding.ActivityDetailNewsBinding
import rizka.bcas.projectsavira.model.ListModel

class DetilNewsListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailNewsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setDatatViewListDetil()
        binding.componenAppBar.ivBack.setOnClickListener{
            this.onBackPressed()
        }
    }
    private fun setDatatViewListDetil(){
        val data=intent.getParcelableExtra<ListModel>(DATA_NEWS_LIST)
        if (data != null) {
            binding.ivDetilNews.setImageResource( data.image?:0)
        }
        binding.tvDetilTitleNews.text=data?.title
        binding.tvDetilDescriptionNews.text=data?.subtitle
        binding.componenAppBar.tvAppbar.text=data?.title
        binding.componenAppBar.ivProfile.visibility= View.GONE

    }
    companion object {
        const val DATA_NEWS_LIST = "dataNewsList"
        fun navigateToActivityListDetil(
            activity: Activity, dataNewsList: ListModel) {
            val intent = Intent(activity, DetilNewsListActivity::class.java)
            intent.putExtra(DATA_NEWS_LIST, dataNewsList)
            activity.startActivity(intent)
        }
    }
}