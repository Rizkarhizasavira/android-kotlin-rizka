package rizka.bcas.projectsavira

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import rizka.bcas.projectsavira.databinding.ActivityDetailNewsBinding
import rizka.bcas.projectsavira.model.NewsModel

class DetailNewsActivity: AppCompatActivity() {
    private lateinit var binding: ActivityDetailNewsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityDetailNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setDatatViewDetil()
        binding.componenAppBar.ivBack.setOnClickListener{
            this.onBackPressed()
        }

    }
    private fun setDatatViewDetil(){
        val data=intent.getParcelableExtra<NewsModel>(DATA_NEWS)
        if (data != null) {
            binding.ivDetilNews.setImageResource( data.image?:0)
        }
        binding.tvDetilTitleNews.text=data?.title
        binding.tvDetilDescriptionNews.text=data?.subtitle
        binding.componenAppBar.tvAppbar.text=data?.title
        binding.componenAppBar.ivProfile.visibility= View.GONE

    }

    companion object{
        /**
         * scooping function
         * **/
        const val DATA_NEWS="dataNews"
        fun navigateToActivityDetil(
            activity: Activity, dataNews:NewsModel){
            val inten= Intent(activity,DetailNewsActivity::class.java)
            inten.putExtra(DATA_NEWS,dataNews)
            activity.startActivity(inten)
        }
    }
}