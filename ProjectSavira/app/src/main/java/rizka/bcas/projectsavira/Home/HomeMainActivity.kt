package rizka.bcas.projectsavira.Home

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import rizka.bcas.projectsavira.*
import rizka.bcas.projectsavira.databinding.ActivityHomeBinding
import rizka.bcas.projectsavira.databinding.ActivityMainBinding
import rizka.bcas.projectsavira.model.ListModel
import rizka.bcas.projectsavira.model.NewsModel

class HomeMainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val name=intent.getStringExtra(LoginActivity.KEY_EMAIL)

        val mainAdapter=HomeMainAdapter(
            dataNews = populateData(),
            onclickNews = {
                DetailNewsActivity.navigateToActivityDetil(this,it)
            }
        )
        binding..adapter=mainAdapter

        val mainAdapterList=HomeMainListAdapter(
            dataNewsList = populateDataLis(),
            onclickNews = {
                DetilNewsListActivity.navigateToActivityListDetil(this,it)
            }
        )
        binding.rvNewsLatest.adapter=mainAdapterList

        binding.componentAppBar.ivProfile.setOnClickListener {
//            val email=binding.inputEmail.text.toString()
//            val password=binding.inputPassword.text.toString()
            navigationScreenWithInput(BiodataActivity::class.java,"Rizka@gmail.com","1234")
        }

        binding.componentAppBar.tvAppbar.text="Hi! $name"
//        binding.rvNews2.adapter=mainAdapter
//        binding.rvNews3.adapter=mainAdapter
        binding.componentAppBar.ivBack.setOnClickListener{
            this.onBackPressed()
        }
    }
    private fun navigationScreenWithInput(screen: Class<*>, inputEmail:String, inputPassword:String){
        val intent= Intent(applicationContext,screen)
        intent.putExtra(LoginActivity.KEY_EMAIL,inputEmail)
        intent.putExtra(LoginActivity.KEY_PASSWORD,inputPassword)
        startActivity(intent)

    }
    private fun populateData():List<NewsModel>{
        val listData= listOf(
            NewsModel(
                image = R.drawable.bcas_content_1,
                icon = R.drawable.bcas_content_1,
                title = "Tarik tunai Cardless di ATM BCA",
                subtitle = "Bagi Anda yang ingin belanja melalui Shopee dan Tokopedia maka bis mendapatkan promo dan diskon."

            ),
            NewsModel(
                image = R.drawable.bcas_content_2,
                icon = R.drawable.bcas_content_2,
                title = "Daftar Promo HUT BCA ke-66: Sushi Tei-Chatime Rp6 Ribu",
                subtitle = "Berikut daftar promo HUT BCA ke-66 yang diberikan untuk pembelian makanan, minuman, hingga transaksi e-commerce."
            ),
            NewsModel(
                image = R.drawable.plane,
                icon = R.drawable.plane,
                title = "Gebyar HUT ke-66 BCA, Begini Cara Dapat Tiket Pesawat Murah",
                subtitle = "Sriwijaya Air memberi diskon besar-besaran pada momen HUT ke-66 BCA. Segini harga tiketnya."
            ),

            )
        return listData

    }
    private fun populateDataLis():List<ListModel>{
        val listData= listOf(
            ListModel(
                image = R.drawable.bcas_content_3,
                title = "Pakai BCA, Ada Diskon 66 Persen di Shopee dan Tokopedia",
                subtitle = "Bagi Anda yang ingin belanja melalui Shopee dan Tokopedia maka bis mendapatkan promo dan diskon."

            ),
            ListModel(
                image = R.drawable.bcas_content_4,
                title = "Daftar Promo HUT BCA ke-66: Sushi Tei-Chatime Rp6 Ribu",
                subtitle = "Berikut daftar promo HUT BCA ke-66 yang diberikan untuk pembelian makanan, minuman, hingga transaksi e-commerce."
            ),
            ListModel(
                image = R.drawable.plane,
                title = "Gebyar HUT ke-66 BCA, Begini Cara Dapat Tiket Pesawat Murah",
                subtitle = "Sriwijaya Air memberi diskon besar-besaran pada momen HUT ke-66 BCA. Segini harga tiketnya."
            ),

            )
        return listData

    }
}