package rizka.bcas.projectsavira.Home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rizka.bcas.projectsavira.databinding.ItemNewsLatestBinding
import rizka.bcas.projectsavira.model.ListModel

class HomeMainListAdapter(
    private val dataNewsList: List<ListModel>,
    private val onclickNews: (ListModel) -> Unit,
) :
    RecyclerView.Adapter<HomeMainListAdapter.HomeMainViewOlder>() {
    inner class HomeMainViewOlder(val binding: ItemNewsLatestBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data:ListModel,onclickNews:(ListModel)->Unit) {
            binding.ivItemNewsIconLates.setImageResource(data.image?:0)
            binding.tvTitleCardNewsLates.text=data.title
            binding.tvDescCardNewsLatest.text=data.subtitle
            binding.cvItemList.setOnClickListener {
                onclickNews(data)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeMainListAdapter.HomeMainViewOlder =
        HomeMainViewOlder(
            ItemNewsLatestBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun getItemCount():Int=dataNewsList.size

    override fun onBindViewHolder(holder:HomeMainListAdapter.HomeMainViewOlder, position: Int) {
        holder.bindView(dataNewsList[position],onclickNews)
    }


}