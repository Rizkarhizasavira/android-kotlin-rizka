package com.bcas.latihan_viewpager.fragment.foodmenu

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FoodModel(
    val image: String?,
    val title: String?,
    val description: String?,
    val category: String?
) : Parcelable
