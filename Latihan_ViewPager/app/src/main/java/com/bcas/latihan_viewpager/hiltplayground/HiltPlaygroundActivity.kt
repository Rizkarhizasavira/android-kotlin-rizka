package com.bcas.latihan_viewpager.hiltplayground

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bcas.latihan_viewpager.R
import com.bcas.latihan_viewpager.databinding.ActivityHiltPlaygroundBinding
import com.bcas.latihan_viewpager.fragment.foodmenu.Engine
import com.bcas.latihan_viewpager.fragment.foodmenu.FoodModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HiltPlaygroundActivity : AppCompatActivity() {

    private var _binding: ActivityHiltPlaygroundBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var engine: Engine

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityHiltPlaygroundBinding.inflate(layoutInflater)
        setContentView(binding.root)

        engine.startEngine()

//        val engine = Engine()
//        engine.startEngine()
    }
}