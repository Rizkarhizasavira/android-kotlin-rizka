package com.bcas.latihan_viewpager.fragment


data class CategoryFoodModel(
    val id: Int?,
    val title: String?,
    val isSelected: Boolean?
)
