package com.bcas.latihan_viewpager.fragment.foodmenu

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bcas.latihan_viewpager.databinding.FragmentCallBinding
import com.bcas.latihan_viewpager.databinding.FragmentFoodMenuBinding
import com.bcas.latihan_viewpager.fragment.CategoryFoodModel
import com.example.myapplication1.view.home.FoodAdapter
import com.example.myfirstmobile.view.home.CategoryFoodAdapter
import java.util.Objects

class FoodMenuFragment : Fragment() {
    private var binding: FragmentFoodMenuBinding? = null
    private var foodAdapter = FoodAdapter()
    private var categoryFoodAdapter = CategoryFoodAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFoodMenuBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        foodAdapter.setData(populateData())
        categoryFoodAdapter.setData(populateDataCategory())

        binding?.rvFood?.adapter = foodAdapter
        binding?.rvCategoryFood?.adapter = categoryFoodAdapter

        categoryFoodAdapter.addOnClickFoodCategoryItem { CategoryFoodModel ->
            val categoryFoodData = populateDataCategory()
            val category = categoryFoodData.map {
                val categoryId = CategoryFoodModel.id
                val isSelected = it.id == categoryId
                it.copy(isSelected = isSelected)
            }
            categoryFoodAdapter.setData(category.toMutableList())

            val data = populateData()
            val filterData = data.filter { dataFood -> dataFood.category == CategoryFoodModel.title }
            foodAdapter.setData(filterData.toMutableList())
        }

        binding?.etSearch?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val data = populateData()
                val filterData = data.filter { dataFood ->
                    (dataFood.title?.contains(s.toString(), ignoreCase = true) ?: false
                            || dataFood.category?.contains(
                        s.toString(),
                        ignoreCase = true
                    ) ?: false)
                }

                foodAdapter.setData(filterData.toMutableList())


                val editTextValue = binding?.etSearch?.text
                if (editTextValue?.isEmpty() == true) {
                    binding?.ivClose?.visibility = View.INVISIBLE
                } else {
                    binding?.ivClose?.visibility = View.VISIBLE
                }
            }

        })

        binding?.ivClose?.setOnClickListener {
            binding?.etSearch?.setText("")
        }
    }


    private fun populateDataCategory(): MutableList<CategoryFoodModel> {
        val listData = mutableListOf(
            CategoryFoodModel(id = 1, title = "manis", isSelected = false),
            CategoryFoodModel(id = 2, title = "asam", isSelected = false),
            CategoryFoodModel(id = 3, title = "pedas", isSelected = false),
            CategoryFoodModel(id = 4, title = "asin", isSelected = false)
        )
        return listData
    }

    private fun populateData(): MutableList<FoodModel> {
        val listData = mutableListOf(
            FoodModel(
                image = "https://assets-pergikuliner.com/4o-dKncqKNS6QQbFDqM52WkrK0o=/fit-in/1366x768/smart/filters:no_upscale()/https://assets-pergikuliner.com/uploads/bootsy/image/12013/picture-1537766225.jpeg",
                title = "1",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "pedas"
            ),
            FoodModel(
                image = "https://assets-pergikuliner.com/4o-dKncqKNS6QQbFDqM52WkrK0o=/fit-in/1366x768/smart/filters:no_upscale()/https://assets-pergikuliner.com/uploads/bootsy/image/12013/picture-1537766225.jpeg",
                title = "2",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "asin"
            ),
            FoodModel(
                image = "https://assets-pergikuliner.com/4o-dKncqKNS6QQbFDqM52WkrK0o=/fit-in/1366x768/smart/filters:no_upscale()/https://assets-pergikuliner.com/uploads/bootsy/image/12013/picture-1537766225.jpeg",
                title = "3",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "manis"
            ),
            FoodModel(
                image = "https://assets-pergikuliner.com/4o-dKncqKNS6QQbFDqM52WkrK0o=/fit-in/1366x768/smart/filters:no_upscale()/https://assets-pergikuliner.com/uploads/bootsy/image/12013/picture-1537766225.jpeg",
                title = "4",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "asam"
            ),
            FoodModel(
                image = "https://assets-pergikuliner.com/4o-dKncqKNS6QQbFDqM52WkrK0o=/fit-in/1366x768/smart/filters:no_upscale()/https://assets-pergikuliner.com/uploads/bootsy/image/12013/picture-1537766225.jpeg",
                title = "5",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "pedas"
            ),
            FoodModel(
                image = "https://assets-pergikuliner.com/4o-dKncqKNS6QQbFDqM52WkrK0o=/fit-in/1366x768/smart/filters:no_upscale()/https://assets-pergikuliner.com/uploads/bootsy/image/12013/picture-1537766225.jpeg",
                title = "6",
                description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "asin"
            ),

            )
        return listData
    }


    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}