package com.example.myapplication1.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bcas.latihan_viewpager.databinding.ItemFoodBinding
import com.bcas.latihan_viewpager.fragment.CategoryFoodModel
import com.bumptech.glide.Glide
import com.bcas.latihan_viewpager.fragment.foodmenu.FoodModel


class FoodAdapter :
    RecyclerView.Adapter<FoodAdapter.FoodViewHolder>() {

    private var dataFood : MutableList<FoodModel> = mutableListOf()
//    private var onClickFood: (FoodModel) -> Unit = {}

//    fun addDataFood(newData: List<FoodModel>){
//        dataFood.addAll(newData)
//        notifyDataSetChanged()
//    }
//
//    fun addOnClickFood(clickNews: (FoodModel) -> Unit){
//        onClickFood = clickNews
//    }

    fun setData(newData: MutableList<FoodModel>){
        dataFood = newData
        notifyDataSetChanged()
    }
    inner class FoodViewHolder(val binding: ItemFoodBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data: FoodModel) {
//            binding.ivItemFood.setImageResource(data.image ?: 0)

            Glide.with(binding.root.context)
                .load(data.image)
                .into(binding.ivItemFood)

            binding.tvTitleFood.text = data.title
            binding.tvFood.text = data.description

//            binding.llNews.setOnClickListener {
//
//            }


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder =
        FoodViewHolder(
            ItemFoodBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        println("render position-> $position")
        holder.bindView(dataFood[position])

    }

    override fun getItemCount(): Int = dataFood.size

}