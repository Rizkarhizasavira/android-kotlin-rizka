package com.example.myfirstmobile.view.home

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bcas.latihan_viewpager.R
import com.bcas.latihan_viewpager.databinding.ItemFoodCategoryBinding
import com.bcas.latihan_viewpager.fragment.CategoryFoodModel

class CategoryFoodAdapter() : RecyclerView.Adapter<CategoryFoodAdapter.CategoryFoodViewHolder>(){

    private var data: MutableList<CategoryFoodModel> = mutableListOf()

    private var OnClickList: (CategoryFoodModel)->Unit={}

    fun setData(newData: MutableList<CategoryFoodModel>){
        data= newData
        notifyDataSetChanged()
    }

    fun addOnClickFoodCategoryItem(clickFood: (CategoryFoodModel)->Unit){
        OnClickList = clickFood
    }

    inner class CategoryFoodViewHolder(val binding: ItemFoodCategoryBinding):
        RecyclerView.ViewHolder(binding.root){
        fun bindingData(data: CategoryFoodModel) {
            binding.tvFoodCategory.text = data.title
            binding.constraintFood.setOnClickListener{
                OnClickList(data)
            }

            val (selectedBackgroundRes, selectedColor) = if (data.isSelected ?:false){
                Pair(R.drawable.background_rounded_selected, Color.CYAN)
            } else {
                Pair(R.drawable.background_rounded_outline_black, Color.BLACK)
            }

            val selectedBackground = ContextCompat.getDrawable(binding.root.context, selectedBackgroundRes)

            binding.constraintFood.background = selectedBackground
            binding.tvFoodCategory.setTextColor(ColorStateList.valueOf(selectedColor))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryFoodViewHolder = CategoryFoodViewHolder(
        ItemFoodCategoryBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: CategoryFoodViewHolder, position: Int) {
        holder.bindingData(data[position])
    }
}