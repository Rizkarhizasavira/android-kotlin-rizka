package com.bcas.latihan_viewpager.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bcas.latihan_viewpager.databinding.FragmentChatBinding
import com.bcas.latihan_viewpager.databinding.FragmentStatusBinding

class StatusFragment : Fragment() {

    private var binding: FragmentStatusBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStatusBinding.inflate(
            inflater, container, false
        )
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }


}