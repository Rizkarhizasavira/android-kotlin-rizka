package com.bcas.latihan_viewpager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bcas.latihan_viewpager.databinding.ActivityMainBinding
import com.bcas.latihan_viewpager.databinding.FragmentStatusBinding
import com.bcas.latihan_viewpager.fragment.CallFragment
import com.bcas.latihan_viewpager.fragment.ChatFragment
import com.bcas.latihan_viewpager.fragment.MyViewPagerAdapter
import com.bcas.latihan_viewpager.fragment.StatusFragment
import com.bcas.latihan_viewpager.fragment.foodmenu.FoodMenuFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var myViewPagerAdapter: MyViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val chatFragment = ChatFragment()
        val statusFragment = StatusFragment()
//        val callFragment = CallFragment()
        val foodFragment = FoodMenuFragment()

        myViewPagerAdapter = MyViewPagerAdapter(supportFragmentManager)

        myViewPagerAdapter.addFragment(chatFragment, "Makanan")
        myViewPagerAdapter.addFragment(statusFragment, "Minumam")
//        myViewPagerAdapter.addFragment(callFragment, "Tips&Tricks")
        myViewPagerAdapter.addFragment(foodFragment,"Foodmenu")

        binding.vpMain.adapter = myViewPagerAdapter
        binding.tlMain.setupWithViewPager(binding.vpMain)

    }
}
