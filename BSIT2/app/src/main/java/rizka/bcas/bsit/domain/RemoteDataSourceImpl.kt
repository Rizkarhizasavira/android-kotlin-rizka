package rizka.bcas.bsit.domain


import retrofit2.Response
import rizka.bcas.bsit.model.ContactResponse
import rizka.bcas.bsit.model.ProfileResponse
import rizka.bcas.bsit.model.TransactionResponse
import javax.inject.Inject

class RemoteDataSourceImpl @Inject constructor(
    private val service: Service
):RemoteDataSource {
    override suspend fun getTransaction(): Response<List<TransactionResponse>> =
        service.getTransaction()

    override suspend fun getContact(): Response<List<ContactResponse>> =
        service.getContact()

    override suspend fun getProfile(): Response<ProfileResponse> = service.getProfile()
}