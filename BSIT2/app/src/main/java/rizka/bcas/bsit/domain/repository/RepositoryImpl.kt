package rizka.bcas.bsit.domain.repository

import retrofit2.Response
import rizka.bcas.bsit.domain.RemoteDataSource
import rizka.bcas.bsit.model.ContactResponse
import rizka.bcas.bsit.model.ProfileResponse
import rizka.bcas.bsit.model.TransactionResponse
import javax.inject.Inject

class RepositoryImpl  @Inject constructor(
        private val  remoteDataSource: RemoteDataSource

) : Repository {
    override suspend fun getTransaction(): Response<List<TransactionResponse>> = remoteDataSource.getTransaction()

        override suspend fun getContact(): Response<List<ContactResponse>> =
            remoteDataSource.getContact()


    override suspend fun getProfile(): Response<ProfileResponse> = remoteDataSource.getProfile()
}
