package rizka.bcas.bsit.domain.useCase

import retrofit2.Response
import rizka.bcas.bsit.domain.repository.Repository
import rizka.bcas.bsit.model.ContactResponse
import rizka.bcas.bsit.model.TransactionResponse
import javax.inject.Inject

class GetContactUseCase @Inject constructor(
    private val repository: Repository
) {
    suspend fun getContact():Response<List<ContactResponse>>{
        return repository.getContact()
    }
}