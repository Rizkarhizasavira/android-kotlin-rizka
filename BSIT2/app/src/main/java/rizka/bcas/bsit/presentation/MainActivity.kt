package rizka.bcas.bsit.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import rizka.bcas.bsit.databinding.ActivityMainBinding
import rizka.bcas.bsit.fragment.ContactFragment
import rizka.bcas.bsit.model.ContactResponse
import rizka.bcas.bsit.model.TransactionResponse
import rizka.bcas.bsit.presentation.adapter.BsitFragmentPagerAdapter
import rizka.bcas.bsit.presentation.adapter.TransactionAdapter
import rizka.bcas.bsit.presentation.viewmodel.MainViewModel


//@AndroidEntryPoint
//class MainActivity : AppCompatActivity() {
//
//    private val mainViewModel by viewModels<MainViewModel>()
//    private lateinit var binding: ActivityMainBinding
//    private lateinit var viewPagerAdapter: BsitFragmentPagerAdapter
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        binding = ActivityMainBinding.inflate(layoutInflater)
//        setContentView(binding.root)
//        setUpViewPager(listOf())
//
//        setUpTableLayout()
//        observeViewModel()
//        mainViewModel.getTransaction()
//        mainViewModel.getContact()
//    }
//
//    private fun setData(dataTransaction: List<TransactionResponse>?, dataContact: List<ContactResponse>){
//        val bundle = listOf(
//            dataContact.let { ContactFragment.createInstance(it) }
//        )
//        setUpViewPager(bundle)
//    }
//
//    private fun setUpViewPager(bundle: List<Bundle>){
//        val viewPager = binding.vpContainer
//        viewPager.adapter = BsitFragmentPagerAdapter(this, bundle)
//
//    }
//    private fun setUpTableLayout(){
//        val tableLayout = binding.tabsFragment
//        val viewPager = binding.vpContainer
//        TabLayoutMediator(tableLayout, viewPager){ tab, position ->
//            when(position){
//                0 -> tab.text = "Transaction"
//                1 -> tab.text = "Contact"
//                2 -> tab.text = "Profile"
//            }
//        }.attach()
//
//    }
//
//    private fun observeViewModel() {
//        mainViewModel.transaction.observe(this) {
////            setDataForTransaction(it)
//        }
//        mainViewModel.errorMessage.observe(this) {
//            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
//        }
//        mainViewModel.showLoading.observe(this){isShow->
//            if(isShow) showLoading()else hideLoading()
//
//        }
//    }
//
//
//    private fun setDataForTransaction(data: List<TransactionResponse>) {
//        val adapter = TransactionAdapter(data)
////        binding.rvMain.adapter = adapter
////        binding.rvMain.layoutManager = LinearLayoutManager(this)
//
//    }
//
//    private fun showLoading(){
//        binding.cmpLoading.root.visibility = View.VISIBLE
////        binding.rvMain.visibility = View.GONE
//    }
//    private fun hideLoading(){
//        binding.cmpLoading.root.visibility = View.GONE
////        binding.rvMain.visibility = View.VISIBLE
//
//    }
//}

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val mainViewModel by viewModels<MainViewModel>()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViewPager(listOf())
        setUpTabLayout()
        observeViewModel()
        mainViewModel.getTransaction()
        mainViewModel.getContact()
    }

    private fun setUpViewPager(bundle: List<Bundle>) {
        val viewPager = binding.vpContainer
        viewPager.adapter = BsitFragmentPagerAdapter(this, bundle)
    }

    private fun setUpTabLayout() {
        val tabLayout = binding.tabsFragment
        val viewPager = binding.vpContainer
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when(position) {
                0 -> tab.text = "Transaction"
                1 -> tab.text = "Contact"
                2 -> tab.text = "Profile"
            }
        }.attach()
    }

    private fun observeViewModel() {
        mainViewModel.transaction.observe(this) {
//            setDataForTransaction(it)
        }
        mainViewModel.errorMessage.observe(this) {
            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
        }
        mainViewModel.showLoading.observe(this) { isShow ->
            if (isShow) showLoading() else hideLoading()
        }
    }

    private fun setDataForTransaction(data: List<TransactionResponse>) {
        val adapter = TransactionAdapter(data)
//        binding.rvListTransaction.adapter = adapter
    }

    private fun showLoading() {
        binding.cmpLoading.root.visibility = View.VISIBLE
//        binding.rvListTransaction.visibility = View.GONE
    }

    private fun hideLoading() {
        binding.cmpLoading.root.visibility = View.GONE
//        binding.rvListTransaction.visibility = View.VISIBLE
    }

}