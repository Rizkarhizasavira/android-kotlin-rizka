package rizka.bcas.bsit.domain


import retrofit2.Response
import rizka.bcas.bsit.model.ContactResponse
import rizka.bcas.bsit.model.ProfileResponse
import rizka.bcas.bsit.model.TransactionResponse

interface RemoteDataSource {
    suspend fun getTransaction():Response<List<TransactionResponse>>
    suspend fun getContact():Response<List<ContactResponse>>
    suspend fun getProfile():Response<ProfileResponse>
}