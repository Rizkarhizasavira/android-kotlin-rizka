package rizka.bcas.bsit.domain.useCase

import retrofit2.Response
import rizka.bcas.bsit.domain.repository.Repository
import rizka.bcas.bsit.model.ProfileResponse
import rizka.bcas.bsit.model.TransactionResponse
import javax.inject.Inject

class GetProfileUseCase @Inject constructor(
    private val repository: Repository
) {
    suspend fun getProfile():Response<ProfileResponse>{
        return repository.getProfile()
    }
}