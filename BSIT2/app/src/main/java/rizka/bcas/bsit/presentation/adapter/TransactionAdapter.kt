package rizka.bcas.bsit.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import rizka.bcas.bsit.R
import rizka.bcas.bsit.databinding.ItemTransactionBinding
import rizka.bcas.bsit.model.TransactionResponse

class TransactionAdapter(
    private val data: List<TransactionResponse>
): RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>(){


    inner class TransactionViewHolder(val binding: ItemTransactionBinding):
    RecyclerView.ViewHolder(binding.root){

        fun bindingView(item: TransactionResponse){
            binding.ccTvTitle.text = item.name
            binding.ccTvSubtitle.text = item.metodeTrf
            binding.ccTvAmount.text = item.nominalSaldo
            Glide
                .with(binding.root.context)
                .load(item.imageUrl)
                .centerCrop()
                .into(binding.ccImage)


            if (item.flagDebitCredit == 1) {
                binding.ccTvAmount.setTextColor(
                    binding.root.context.resources.getColor(R.color.red)
                )
            } else {
                binding.ccTvAmount.setTextColor(
                    binding.root.context.resources.getColor(R.color.green)
                )
            }
        }
    }

   override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
       val inflate = LayoutInflater.from(parent.context)
       val bind = ItemTransactionBinding.inflate(inflate, parent,false)
       return TransactionViewHolder(bind)
   }

    override fun getItemCount()= data.size


    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bindingView(data[position])
    }

}