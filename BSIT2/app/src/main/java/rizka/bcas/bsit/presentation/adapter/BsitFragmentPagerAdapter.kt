package rizka.bcas.bsit.presentation.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import rizka.bcas.bsit.fragment.ContactFragment
import rizka.bcas.bsit.fragment.ProfileFragment
import rizka.bcas.bsit.fragment.TransactionFragment

class BsitFragmentPagerAdapter(fragment: FragmentActivity, private val bundle: List<Bundle>)
    : FragmentStateAdapter(
    fragment
    ){

    private val fragment = listOf(
        ::TransactionFragment,
        ::ContactFragment,
        ::ProfileFragment
    )

    override fun getItemCount() = fragment.size


    override fun createFragment(position: Int): Fragment {
        val fragmentName = fragment.getOrNull(position)
        val fragmentInstance = fragmentName?.invoke() as? Fragment ?: Fragment()

        val bundle = bundle.getOrNull(position)
        fragmentInstance.arguments = bundle

        return fragmentInstance
    }
}