package rizka.bcas.bsit.domain


import retrofit2.Response
import retrofit2.http.GET
import rizka.bcas.bsit.model.ContactResponse
import rizka.bcas.bsit.model.ProfileResponse
import rizka.bcas.bsit.model.TransactionResponse

interface Service {
    /**
        url
        https://private-54eacf-fazztrack.apiary-mock.com/questions
    **/
    @GET("transaction")
    suspend fun getTransaction():Response<List<TransactionResponse>>

    @GET("contact")
    suspend fun getContact():Response<List<ContactResponse>>

    @GET("profile")
    suspend fun getProfile():Response<ProfileResponse>


}