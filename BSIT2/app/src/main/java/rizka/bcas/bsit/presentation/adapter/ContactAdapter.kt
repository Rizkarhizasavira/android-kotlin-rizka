package rizka.bcas.bsit.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rizka.bcas.bsit.databinding.ContactItemBinding
import rizka.bcas.bsit.model.ContactResponse

class ContactAdapter() : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>(){

    inner class ContactViewHolder(private val binding: ContactItemBinding):
            RecyclerView.ViewHolder(binding.root){

                fun bindingView(item: ContactResponse){
                    binding.ccTvTitle.text = item.name
                    binding.tvPhone.text = item.noTelp
                    binding.ivCall.setOnClickListener { onClickPhone.invoke(item)}
                }
            }

    private var data: MutableList<ContactResponse> = mutableListOf()
    private var onClickPhone: (ContactResponse) -> Unit = {}

    fun onClickIconCall(clickContact:(ContactResponse)->Unit){
        onClickPhone = clickContact
    }

    fun setData(newData: MutableList<ContactResponse>) {
        data = newData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ContactViewHolder(
        ContactItemBinding.inflate(LayoutInflater.from(parent.context), parent,false)
    )


    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bindingView(data[position])
    }

}
