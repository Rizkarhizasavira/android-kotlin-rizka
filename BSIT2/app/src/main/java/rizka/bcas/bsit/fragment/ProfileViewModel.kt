package rizka.bcas.bsit.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import rizka.bcas.bsit.domain.useCase.GetContactUseCase
import rizka.bcas.bsit.domain.useCase.GetProfileUseCase
import rizka.bcas.bsit.domain.useCase.GetTransactionUseCase
import rizka.bcas.bsit.model.ContactResponse
import rizka.bcas.bsit.model.ProfileResponse
import rizka.bcas.bsit.model.TransactionResponse
import javax.inject.Inject


@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val getProfileUseCase: GetProfileUseCase
) : ViewModel() {

    private val _profile = MutableLiveData<ProfileResponse>()
    val profile: LiveData<ProfileResponse> get() = _profile

    private val _showLoading = MutableLiveData<Boolean>()
    val showLoading: LiveData<Boolean> get() = _showLoading

    fun getProfile() = viewModelScope.launch {
        _showLoading.postValue(true)
        getProfileUseCase.getProfile().let {
            if (it.isSuccessful) {
                _profile.postValue(it.body())
                _showLoading.postValue(false)
            } else {
                _showLoading.postValue(false)
            }
        }
    }
}