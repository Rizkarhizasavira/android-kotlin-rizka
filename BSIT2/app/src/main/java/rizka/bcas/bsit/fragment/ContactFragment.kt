package rizka.bcas.bsit.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import rizka.bcas.bsit.databinding.FragmentContactBinding
import rizka.bcas.bsit.databinding.FragmentTransactionBinding
import rizka.bcas.bsit.model.ContactResponse
import rizka.bcas.bsit.presentation.adapter.ContactAdapter

@AndroidEntryPoint
class ContactFragment : Fragment() {


    private val viewModel: ContactViewModel by viewModels()

    private var contactAdapter = ContactAdapter()
    private var _binding: FragmentContactBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentContactBinding.inflate(inflater, container, false)

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeViewModel()
        viewModel.getContact()

        binding.etSearch.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                val editTextValue = binding.etSearch.text
                if (editTextValue.isEmpty()){
                    binding.ivClose.visibility = View.INVISIBLE
                }else{
                    binding.ivClose.visibility = View.VISIBLE
                }
                viewModel.filterSearchContact(p0.toString())
            }
        })

        binding.ivClose.setOnClickListener{
            binding.etSearch.text.clear()
        }

    }

    private fun observeViewModel() {
        viewModel.contact.observe(viewLifecycleOwner) { contactData ->
            if (contactData != null) {
                setData(contactData)
            }
        }
    }

    private fun setData(data: List<ContactResponse>) {
        contactAdapter.setData(data.toMutableList())
        contactAdapter.onClickIconCall {
            callAFriend(it.noTelp)
        }
        binding?.rvContact?.adapter = contactAdapter
    }

    private fun callAFriend(phoneNumber: String?) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNumber")
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}