package rizka.bcas.bsit.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ContactResponse(
    @SerializedName("name")
    val name: String?,
    @SerializedName("no_telp")
    val noTelp:String?
):Parcelable
