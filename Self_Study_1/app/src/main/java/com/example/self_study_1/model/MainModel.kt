package com.example.self_study_1.model

data class MainModel (
    val title: String,
    val movieModels: List<MovieModel>
)