package com.example.self_study_1

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.self_study_1.databinding.ActivityLoginBinding

//class LoginActivity : AppCompatActivity() {
//    private lateinit var binding: ActivityLoginBinding
//
//    lateinit var username: EditText
//    lateinit var password: EditText
//    lateinit var loginButton: Button
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        binding = ActivityLoginBinding.inflate(layoutInflater)
//        setContentView(binding.root)
//
//        binding.loginButton.setOnClickListener {
//            if (binding.username.text.toString() == "user" && binding.password.text.toString() == "1234") {
//
//                Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show()
//
//            } else {
//
//                Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show()
//            }
//        }
//
//    }
//
//}

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    private val email = "ir@gmail.com"
    private val name = "name"
    private val address = "address"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.loginButton.setOnClickListener {
            val inputEmail = binding.username.text.toString()
            navigateScreenwithInput(ProfileActivity::class.java, inputEmail)
            if (inputEmail == email) {
                Toast.makeText(applicationContext, "Berhasil Login", Toast.LENGTH_SHORT).show()
                navigateScreen(ProfileActivity::class.java)
            } else {
                Toast.makeText(applicationContext, "Gagal Login", Toast.LENGTH_SHORT).show()
            }

            showLoading()
        }

        binding.pbLoading.setOnClickListener{
            hideLoading()
        }
    }

    private fun navigateScreen (screen: Class<*>) {
        val intent = Intent(applicationContext, screen)
        intent.putExtra(KEY_NAME, name)
        intent.putExtra(KEY_ADDRESS, address)
        startActivity(intent)
    }

    private fun showLoading() {

        binding.pbLoading.visibility = View.VISIBLE
        binding.loginButton.visibility = View.GONE
    }

    private fun hideLoading() {

        binding.loginButton.visibility = View.GONE
        binding.pbLoading.visibility = View.VISIBLE

    }

    private fun navigateScreenwithInput (screen: Class<*>, input: String) {
        val intent = Intent(applicationContext, screen)
        intent.putExtra(KEY_INPUT, name)
        intent.putExtra(KEY_ADDRESS, address)
        startActivity(intent)
    }

    companion object {
        const val KEY_NAME = "name"
        const val KEY_ADDRESS = "address"
        const val KEY_INPUT = "input"

    }
}

