package com.example.self_study_1.model

data class MovieVerticalModel(
    val imageUrl : Int?,
    val title : String
)