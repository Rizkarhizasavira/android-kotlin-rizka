package com.example.self_study_1.adapter

import android.graphics.Movie
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.self_study_1.R
import com.example.self_study_1.databinding.ItemVerticalBinding
import com.example.self_study_1.databinding.MovieItemBinding
import com.example.self_study_1.model.MovieModel
import com.example.self_study_1.model.MovieVerticalModel

class MovieVerticalAdapter() : RecyclerView.Adapter<MovieVerticalAdapter.MovieViewHolder>() {

    private var movieVerticalData: List<MovieVerticalModel> = listOf()

    fun setData(dataMovie: List<MovieVerticalModel>) {
        movieVerticalData = dataMovie
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder = MovieViewHolder(
        ItemVerticalBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )


    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bindView(movieVerticalData[position])
    }

    override fun getItemCount() = movieVerticalData.size


    inner class MovieViewHolder(val binding: ItemVerticalBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindView(data: MovieVerticalModel) {
            binding.ivMovieVertical.setImageResource(data.imageUrl ?: 0)
            binding.tvTitleMovie.text = data.title
        }

    }

}