package com.example.self_study_1

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.self_study_1.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.include.llHomeworks.setOnClickListener {
            val intent = Intent(this, HomeworkActivity::class.java)
            startActivity(intent)
        }
        binding.include.llLearn.setOnClickListener {
            val intent = Intent(this, LearnActivity::class.java)
            startActivity(intent)
        }

//        binding.include.llProjects.setOnClickListener{
//            val intent = Intent()
//            intent.component = ComponentName("rizka.bcas", "rizka.bcas.bsit")
//            startActivity(intent)
//        }
        binding.include.llProjects.setOnClickListener {
//            val i = Intent(Intent.ACTION_MAIN)
//            i.component = ComponentName("rizka.bcas", "")
//            i.addCategory(Intent.CATEGORY_LAUNCHER)
//            startActivity(i)

//            val launchIntent = packageManager.getLaunchIntentForPackage("rizka.bcas.bsit")
//            launchIntent?.let { startActivity(it) }

            val intent = Intent(Intent.ACTION_MAIN)
            intent.setClassName("rizka.bcas.bsit", "rizka.bcas.bsit.presentation.MainActivity")
            startActivity(intent)
        }




    }
}