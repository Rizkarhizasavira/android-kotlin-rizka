package com.example.self_study_1.model

import com.example.self_study_1.Images
import com.example.self_study_1.R

object SampleData {

    val movieModel= listOf(
        MovieModel(R.drawable.image1),
        MovieModel(R.drawable.image2),
        MovieModel(R.drawable.image3),
        MovieModel(R.drawable.image4),
        MovieModel(R.drawable.image5),
        MovieModel(R.drawable.image6),
        MovieModel(R.drawable.image7),
        MovieModel(R.drawable.image8),
        MovieModel(R.drawable.image9),
        MovieModel(R.drawable.image10)
    )

    val movieVerticalModel = listOf(
        MovieVerticalModel(
            imageUrl = R.drawable.image1h,
            title = "Test"
        ),
        MovieVerticalModel(
            imageUrl = R.drawable.image2h,
            title = "Test"
        ),
        MovieVerticalModel(
            imageUrl = R.drawable.image5h,
            title = "Test"
        ),
        MovieVerticalModel(
            imageUrl = R.drawable.image6h,
            title = "Test"
        ),
        MovieVerticalModel(
            imageUrl = R.drawable.image8h,
            title = "Test"
        ),
        MovieVerticalModel(
            imageUrl = R.drawable.image9h,
            title = "Test"
        ),

    )

    val collections = listOf(
        MainModel("All Movie" , movieModel),
        MainModel("Want to see" , movieModel.reversed()),
        MainModel("Popular Movie" , movieModel.shuffled()),
        MainModel("Top Rated Movie" , movieModel)

    )
}