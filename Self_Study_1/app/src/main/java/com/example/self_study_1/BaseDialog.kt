package com.example.self_study_1

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.core.view.isVisible
import com.example.self_study_1.databinding.BaseDialogBinding

class BaseDialog(
    context: Context,
    private var title: String,
    private var subtitle: String,
    private val onclicked: (() -> Unit),
    private val withImage: Boolean,
    private val image: Int? = null
) : Dialog(context) {

    private lateinit var binding: BaseDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = BaseDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivDialog.setImageResource(image ?: R.drawable.ic_baseline_warning_24)

        binding.ivDialog.isVisible = withImage

        binding.tvDialogTitle.text = title
        binding.tvDialogSubtitle.text = subtitle

//        contoh 1
        binding.btnCancel.setOnClickListener { dismiss() }
        binding.btnProceed.setOnClickListener { onclicked.invoke() }
    }

    //    contoh 2
    private fun dismissDialog() {
        dismiss()
    }
}