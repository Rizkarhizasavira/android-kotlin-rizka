package com.example.self_study_1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.self_study_1.databinding.ActivityLearnBinding


class LearnActivity: AppCompatActivity() {
    private lateinit var binding: ActivityLearnBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLearnBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}