package com.example.self_study_1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.self_study_1.adapter.MovieAdapter
import com.example.self_study_1.adapter.MovieVerticalAdapter
import com.example.self_study_1.databinding.ActivityRecyclerviewBinding
import com.example.self_study_1.model.SampleData

class RecyclerViewActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRecyclerviewBinding
     private var movieAdapter = MovieAdapter()
    private var movieVerticalAdapter = MovieVerticalAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecyclerviewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        movieAdapter.setData(SampleData.movieModel)
        movieVerticalAdapter.setData(SampleData.movieVerticalModel)

        binding.rvRecycler.adapter = movieAdapter
        binding.rvRecyclervertical.adapter = movieVerticalAdapter

    }
}