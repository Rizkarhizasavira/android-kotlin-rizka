package com.example.self_study_1.adapter

import android.graphics.Movie
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.self_study_1.R
import com.example.self_study_1.databinding.MovieItemBinding
import com.example.self_study_1.model.MovieModel

class MovieAdapter() : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private var movieData: List<MovieModel> = listOf()

    fun setData(dataMovie: List<MovieModel>) {
        movieData = dataMovie
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder = MovieViewHolder(
        MovieItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )


    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bindView(movieData[position])
    }

    override fun getItemCount() = movieData.size


    inner class MovieViewHolder(val binding: MovieItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindView(data: MovieModel) {
            binding.imgMoviePoster.setImageResource(data.imageUrl ?: 0)
        }

    }

}