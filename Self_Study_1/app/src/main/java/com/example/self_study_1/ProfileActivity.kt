package com.example.self_study_1

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.self_study_1.LoginActivity.Companion.KEY_ADDRESS
import com.example.self_study_1.LoginActivity.Companion.KEY_INPUT
import com.example.self_study_1.LoginActivity.Companion.KEY_NAME
import com.example.self_study_1.databinding.ActivityProfileBinding

class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val name = intent.getStringExtra(KEY_NAME)
        val address = intent.getStringExtra(KEY_ADDRESS)
        val valueinput = intent.getStringExtra(KEY_INPUT)

        binding.tvName.text = name
        binding.tvAddress.text = address

        logout()
    }

    private fun logout() {
        binding.btnLogout.setOnClickListener {
            val dialog =
                BaseDialog(
                    this, "Peringatan", "yakin ingin keluar aplikasi?", onclicked = {
//                    Toast.makeText(this, "ini button proceed", Toast.LENGTH_SHORT).show()
                        finish()
//                    untuk ke page selanjutnya
                        startActivity(Intent(this, LoginActivity::class.java))
                    },
                    withImage = false,
//                    kalo withimage true dan gapake imagenya itu bakal pake yang default
//                    kalo withimage nya false ga akan keluar imagenya
                    image = R.drawable.ic_launcher_background
                )
            dialog.setCancelable(false)
            dialog.show()
//            BaseDialog(this).setCancelable(false).show()
        }

    }
}