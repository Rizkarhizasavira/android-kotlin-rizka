package com.example.self_study_1

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.self_study_1.databinding.ActivityHomeworkBinding

class HomeworkActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeworkBinding
    private lateinit var listIntent: Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Setup view binding
        binding = ActivityHomeworkBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Launch the VerticalListActivity on verticalBtn click
        binding.profileBtn.setOnClickListener { launchVertical() }

        // Launch the HorizontalListActivity on horizontalBtn click
        binding.recyclerBtn.setOnClickListener { launchHorizontal() }

        // Launch the GridListActivity on gridBtn click
        binding.loginBtn.setOnClickListener { login() }
    }

    private fun launchVertical() {
        listIntent = Intent(this, ProfileActivity::class.java)
        startActivity(listIntent)
    }

    private fun launchHorizontal() {
        listIntent = Intent(this, RecyclerViewActivity::class.java)
        startActivity(listIntent)
    }

    private fun login() {
        listIntent = Intent(this, LoginActivity::class.java)
        startActivity(listIntent)
    }
}